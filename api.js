const btnDogButton = document.querySelector('button');
const dogImageHTML = document.querySelector('img');
const divBoxForPics = document.querySelector('.containerForAllPics');
const inputTag = document.querySelector('input');


//dodanie tagu do html
appendToHTML = (where, what) => {
    where.appendChild(what)
}

//tworzenie obrazka
createIMG = (src) => {
    const divForPic = document.createElement('div');
    const imgTag = document.createElement('img');

    divForPic.classList.add('imgBox');
    imgTag.src = src;

    appendToHTML(divBoxForPics, divForPic)
    appendToHTML(divForPic, imgTag)
}

//reset obrazkow
resetAllDogs = () => {
    const allGeneratedPicks = document.querySelectorAll('.imgBox');
    allGeneratedPicks.forEach(function (pick) {
        pick.remove()
    })

    inputTag.focus()
}


//fetch 
requestDogs = () => {
    for (let i = 0; i < inputTag.value; i++) {
        fetch("https://dog.ceo/api/breeds/image/random")
            .then(resulut => {
                console.log(resulut)
                return resulut.json()
            })
            .then(pic => {
                console.log(pic)
                createIMG(pic.message);
            })
    }
    inputTag.value = "";
    resetAllDogs();
}
//button
btnDogButton.addEventListener('click', requestDogs)